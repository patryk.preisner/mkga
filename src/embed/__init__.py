from .rdf_2_vec import RDF2Vec

from .pykeen_models import TransE,  ComplEx, DistMult, SimplE

